import logging
import json
import pymysql
import os

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection

    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_checkBrandExist(lv_CutitronicsBrandID=None, lv_BrandName=None):

    openConnection()

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, BrandName, ContactName, ContactTitle, ContactEmail FROM BrandDetails WHERE CutitronicsBrandID = %(CutitronicsBrandID)s OR BrandName = %(BrandName)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID, 'BrandName': lv_BrandName})
            Connection.close()
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(500)
            else:
                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 500


def fn_ReturnAllProducts(lv_CutitronicsBrandID):

    openConnection()

    lv_list = []
    lv_statement = "SELECT BrandProductsID, CutitronicsBrandID, CutitronicsSKUCode, CutitronicsSKUCat, CutitronicsSKUType, CutitronicsSKUName, CutitronicsSKUDesc, DefaultURL, DefaultIMG, DefaultVideo, DefaultParameters, RecommendedAmount, DefaultVolume FROM BrandProducts WHERE CutitronicsBrandID = %(CutitronicsBrandID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID})
            Connection.close()
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list.append(dict(zip(field_names, row)))

            if len(lv_list) == 0:
                return(300)
            else:
                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 300


def lambda_handler(event, context):
    """
    getBrandProducts

    """
    logger.info("Lambda function - {function_name}".format(function_name=context.function_name))

    # Variables

    lv_msgVersion = None
    lv_testFlag = None
    lv_CutitronicsBrandID = None

    logger.info(' ')
    logger.info("Payload event - '{event}'".format(event=event))
    logger.info(' ')

    lv_CutitronicsBrandID = event.get('multiValueQueryStringParameters')
    lv_CutitronicsBrandID = lv_CutitronicsBrandID.get('CutitronicsBrandID')

    # Return block

    getBrandProducts_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {}}

    try:

        '''
        1. Does brand exist ?
        '''

        logger.info(' ')
        logging.info("Calling fn_checkBrandExist with the following values CutitronicsBrandID - {CutitronicsBrandID}".format(CutitronicsBrandID=lv_CutitronicsBrandID))
        logger.info(' ')

        lv_BrandType = fn_checkBrandExist(lv_CutitronicsBrandID, lv_CutitronicsBrandID)

        if lv_BrandType == 500:  # Failure

            logger.error("Brand '{lv_CutitronicsBrandID}' has not been found in the back office systems".format(lv_CutitronicsBrandID=lv_CutitronicsBrandID))

            getBrandProducts_out['Status'] = "Error"
            getBrandProducts_out['ErrorCode'] = context.function_name + "_001"  # getBrandProducts_out_001
            getBrandProducts_out['ErrorDesc'] = "Supplied brandID does not exist in the BO Database"

            # Lambda response

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getBrandProducts_out)
            }

        '''
        2. Return Brand Products
        '''

        getBrandProducts_out['ServiceOutput']['BrandProducts'] = {}

        logger.info(' ')
        logging.info("Calling fn_ReturnAllProducts with the following values CutitronicsBrandID - {CutitronicsBrandID}".format(CutitronicsBrandID=lv_CutitronicsBrandID))
        logger.info(' ')

        lv_Products = fn_ReturnAllProducts(lv_CutitronicsBrandID)

        lv_BrandProducts = []
        for q in lv_Products:
            lv_Product = {}

            lv_Product = {
                "SKUCode": q.get('CutitronicsSKUCode'),
                "Category": q.get('CutitronicsSKUCat'),
                "Type": q.get('CutitronicsSKUType'),
                "ProductName": q.get('CutitronicsSKUName'),
                "TextDescription": q.get('CutitronicsSKUDesc'),
                "ImgURL": q.get('DefaultIMG'),
                "DefaultURL": q.get('DefaultURL'),
                "VideoURL": q.get('DefaultVideo'),
                "DefaultParameters": q.get('DefaultParameters'),
                "defaultVolume": q.get('DefaultVolume', None),
                "RecommendedAmount": q.get('RecommendedAmount', None)
            }

            lv_BrandProducts.append(lv_Product)

        logging.info(' ')
        logging.info("Returning the following details to caller - {lv_BrandProducts}".format(lv_BrandProducts=lv_BrandProducts))
        logging.info(' ')

        getBrandProducts_out['ServiceOutput']['BrandProducts'] = lv_BrandProducts

        return {
            "isBase64Encoded": "false",
            "statusCode": 200,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getBrandProducts_out)
        }

    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        # Populate Cutitronics reply block

        getBrandProducts_out['Status'] = "Error"
        getBrandProducts_out['ErrorCode'] = context.function_name + "_000"  # regUser_000
        getBrandProducts_out['ErrorDesc'] = "CatchALL"

        # Lambda response

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getBrandProducts_out)
        }
